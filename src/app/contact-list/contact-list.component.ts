import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { LabelService } from '../label.service';
import { SearchService } from '../search.service';
import { Subscription } from 'rxjs';
import { IContact } from '../contact';
import { delay } from 'rxjs/operators';

declare var UIkit;
@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})

export class ContactListComponent implements OnInit {
  contactView: any;
  searchQuerySub: Subscription;
  contacts: IContact[];

  constructor(
    public contactSerive: ContactService,
    public labelservice: LabelService,
    private searchService: SearchService,
  ) { }

  ngOnInit() {
    console.log('heloo');
    this.searchService.getQuery.pipe(delay(100)).subscribe(console.log);
    this.searchQuerySub = this.searchService.searchQuery.subscribe((term) => {
      console.log(term);
      if (term === '') {
        this.contacts = this.contactSerive.getContact;
      } else {
        this.contacts = this.contactSerive.getContact.filter(c =>
          c.firstName === term || c.middleName === term || c.lastName === term || !!c.emailId.find(e => e.name === term)
        );
      }
    });
  }


  get contact() {
    return this.contactSerive.getContact;
  }

  viewContact(id) {
    console.log('this', id);
    this.contactView = this.contactSerive.viewContact(id);
    UIkit.modal('#gc-view-contact-modal').show();
  }

  get displaylabel() {
    return this.labelservice.getLabel;
  }

  deleteContact(id) {
    this.contactSerive.deleteContact(id);
    UIkit.modal('#gc-view-contact-modal').hide();
  }

  editContact(id) {
    this.contactSerive.editContact(id);
    UIkit.modal('#gc-create-contact-modal').show();
  }
}
