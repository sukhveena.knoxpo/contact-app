import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../search.service';
import { InMemoryDataService } from '../../in-memory-data.service';
import { Observable, Subject, Subscription } from 'rxjs';
import { IContact } from '../../contact';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
import { ContactService } from '../../contact.service';

declare var UIkit;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [SearchService]
})
export class HeaderComponent implements OnInit {
  contactView: any;
  contacts$: Observable<IContact[]>;
  private searchTerms = new Subject<string>();
  searchQuerySub: Subscription;

  constructor(private searchService: SearchService, public contactSerive: ContactService) { }



  ngOnInit(): void {
    // this.contacts$ = this.searchTerms.pipe(
    //   debounceTime(300),

    //   distinctUntilChanged(),

    //   switchMap((term: string) => this.searchService.searchContacts(term)),
    // );
  }

  search(term: string): void {
    this.searchService.query(term);
    // this.searchTerms.next(term);
  }


    viewContact(id) {
    console.log('this', id);
    this.contactView = this.contactSerive.viewContact(id);
    UIkit.modal('#gc-view-contact-modal').show();
    console.log(this.contactView.id);
  }


}
