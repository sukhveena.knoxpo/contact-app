import { Injectable } from '@angular/core';
import { ILabel } from './contact';

@Injectable({
  providedIn: 'root'
})
export class LabelService {

  private label: ILabel[] = [];
  constructor() { }

  addList(listname: string) {
    this.label.push({ id: Date.now(), name: listname });
    console.log(listname);
  }

  get getLabel() {
    return (this.label);
  }
  selectedLabel(id: number) {
    const labelIndex = this.label.findIndex((val) => val.id === id);
    return (this.label[labelIndex].name);
  }
  editLabel(id: number, name: string) {
    const labelIndex = this.label.findIndex((val) => val.id === id);
    this.label[labelIndex].name = name;
  }
  deleteLabel(id: number) {
    const labelIndex = this.label.findIndex((val) => val.id === id);
    this.label.splice(labelIndex, 1);
  }
}
