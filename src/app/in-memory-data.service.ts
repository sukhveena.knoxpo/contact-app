import { InMemoryDbService } from 'angular-in-memory-web-api';
import { IContact } from './contact';
import { Injectable } from '@angular/core';
import { ContactService } from 'src/app/contact.service';
@Injectable({
    providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
    constructor(public cs: ContactService) { }
    createDb() {
        const contacts = this.cs.contact;
        return { contacts };
    }
}
