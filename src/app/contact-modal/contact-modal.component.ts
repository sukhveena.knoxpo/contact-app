import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { ContactService } from '../contact.service';
import { LabelService } from '../label.service';
import { Subscription } from 'rxjs';
import { IPhone, IEmail } from '../contact';

declare var UIkit;
@Component({
  selector: 'app-contact-modal',
  templateUrl: './contact-modal.component.html',
  styleUrls: ['./contact-modal.component.scss']
})
export class ContactModalComponent implements OnInit, OnDestroy {
  userIcon = '';
  morefields = '';
  showMore = false;
  profileForm: FormGroup;
  idSbc: Subscription;
  contactId: number;

  show = false;
  constructor(private fb: FormBuilder, public cs: ContactService, public labelservice: LabelService) { }

  ngOnInit() {
    this.profileForm = this.fb.group({
      firstName: [''],
      profilePhoto: [''],
      lastName: [''],
      middleName: [''],
      company: [''],
      job: [''],
      notes: [''],
      phone: this.fb.array([this.createPhone()]),
      emailId: this.fb.array([this.createEmail()]),
    });
    this.idSbc = this.cs.editContactId.subscribe((id) => {
      console.log(id);
      this.contactId = id;
      if (id === null) {
        this.profileForm.reset();
      } else {
        const contact = this.cs.viewContact(id);
        this.profileForm.setValue({
          firstName: contact.firstName,
          profilePhoto: contact.profilePhoto,
          lastName: contact.lastName,
          middleName: contact.middleName,
          company: contact.company,
          job: contact.job,
          notes: contact.notes,
          phone: this.fb.array(this.mapPhone(contact.phone)),
          emailId: this.fb.array(this.mapEmail(contact.emailId)),
        });
      }
    });
  }

  ngOnDestroy() {
    this.idSbc.unsubscribe();
  }

  createPhone(data?: IPhone): FormGroup {
    return this.fb.group({
      phoneNo: [(data && data.phonenumber) ? data.phonenumber : '', Validators.minLength(10)],
      code: (data && data.code) ? data.code : '',
      label: (data && data.label) ? data.label : ''
    });
  }

  mapPhone(group: IPhone[]) {
    const a = [];
    group.forEach(data => {
      a.push(this.createPhone(data));
    });
    return a;
  }

  mapEmail(group: IEmail[]) {
    const a = [];
    group.forEach(data => {
      a.push(this.createEmail(data));
    });
    return a;
  }


  createEmail(data?: IEmail): FormGroup {
    return this.fb.group({
      email: [(data && data.name) ? data.name : '', Validators.email],
      elabel: (data && data.label) ? data.label : ''
    });
  }

  get phone() {
    return this.profileForm.get('phone') as FormArray;
  }
  addPhone() {

    this.phone.push(this.createPhone());
  }

  get emailId() {
    return this.profileForm.get('emailId') as FormArray;
  }
  addEmail() {
    this.emailId.push(this.createEmail());
  }

  get displaylabel() {
    return this.labelservice.getLabel;
  }

  showIcon() {
    if (this.userIcon === '') {
      this.userIcon = 'ti-camera';
    } else {
      this.userIcon = '';
    }
  }
  moreFields() {
    this.showMore = !this.showMore;
    // this.morefields = 'moreFields';
  }

  onSubmit() {
    this.cs.addcontact(this.profileForm.value);
    this.onClose();
  }

  getphone() {
    this.profileForm.value.phone.forEach(element => {
      return (element.phoneNo);
    });
  }

  onClose() {
    this.profileForm.reset();
    UIkit.modal('#gc-create-contact-modal').hide();
    this.cs.editContact(null);
  }

}

