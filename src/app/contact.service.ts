import { Injectable } from '@angular/core';
import { IContact } from './contact';
import { Subject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ContactService {

  public contact: IContact[] = [];
  private updateId: Subject<number> = new Subject<number>();
  editContactId: Observable<number> = this.updateId.asObservable();

  constructor() { }

  addcontact(contact: IContact) {
    this.contact.push({
      id: Date.now(),
      ...contact
    });
    console.log(this.contact);
  }


  get getContact() {
    return (this.contact);
  }

  viewContact(id) {
    const contactIndex = this.contact.findIndex((val) => val.id === id);
    return (this.contact[contactIndex]);

  }

  deleteContact(id: number) {
    const contactIndex = this.contact.findIndex((val) => val.id === id);
    this.contact.splice(contactIndex, 1);
  }


  editContact(id) {
    this.updateId.next(id);
  }
}
