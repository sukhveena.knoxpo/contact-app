import { Component, OnInit } from '@angular/core';
import { LabelService } from 'src/app/label.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ContactService } from 'src/app/contact.service';

declare var UIkit;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  iconLabel = 'ti-angle-up';
  id: number;
  label: FormGroup;
  constructor(private fb: FormBuilder, public labelservice: LabelService) { }

  ngOnInit() {
    this.label = this.fb.group({
      name: ['']
    });
  }
  changeIcon() {
    if (this.iconLabel === 'ti-angle-up') {
      this.iconLabel = 'ti-angle-down';
    } else {
      this.iconLabel = 'ti-angle-up';
    }
  }

  openModel() {
    // UIkit.modal.dialog('<p>UIkit dialog!</p>');
    UIkit.modal('#gc-create-contact-modal').show();
  //   UIkit.modal(`
  //   <div id="gc-create-contact-modal" class="uk-flex-top uk-modal uk-open">
  //     <div class="uk-modal-dialog gc-contact-modal uk-margin-auto-vertical">
  //       // <app-contact-modal></app-contact-modal>
  //       <p>UIkit dialog!</p>
  //     </div>
  //   </div>
  // `).show();
  }

  labelUpdate(id) {
    this.label.setValue({
      name: this.labelservice.selectedLabel(id)
    });
    UIkit.modal('#gc-edit-label-modal').show();
    this.id = id;
  }
  get displaylabel() {
    return this.labelservice.getLabel;
  }

  editlabel() {
    UIkit.modal('#gc-edit-label-modal').hide();
    this.labelservice.editLabel(this.id, this.label.value.name);
  }

  deletelabel(id) {
    this.labelservice.deleteLabel(id);
  }
}
