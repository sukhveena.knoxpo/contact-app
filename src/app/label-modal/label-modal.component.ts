import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { LabelService } from '../label.service';

declare var UIkit;

@Component({
  selector: 'app-label-modal',
  templateUrl: './label-modal.component.html',
  styleUrls: ['./label-modal.component.scss']
})
export class LabelModalComponent implements OnInit {

  label = this.fb.group({
    name: ['']
  });
  constructor(private fb: FormBuilder, public labelservice: LabelService) {
  }

  ngOnInit() {
  }
  onSubmit() {
    UIkit.modal('#gc-create-label-modal').hide();
    this.labelservice.addList(this.label.value.name);
    this.label.reset();
  }


}
