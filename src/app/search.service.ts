import { Injectable } from '@angular/core';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { IContact } from './contact';
@Injectable({ providedIn: 'root' })
export class SearchService {
  private baseUrl = 'api/contacts';
  private sSearchQuery: Subject<string> = new Subject<string>();
  searchQuery: Observable<string> = this.sSearchQuery.asObservable();


  constructor(private http: HttpClient) {
    // this.searchQuery.subscribe(console.log);
  }

  searchContacts(term: string): Observable<IContact[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<IContact[]>(`${this.baseUrl}/?firstName=${term}`).pipe(
      tap(_ => this.log(`found contacts matching "${term}"`)),
      catchError(this.handleError<IContact[]>('searchContacts', []))
    );
  }

  query(term: string) {
    // console.log(term);
    this.sSearchQuery.next(term);
  }

  get getQuery():Observable<string> {
    return this.sSearchQuery.asObservable();
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  private log(message: string) {
    console.log(`Service: ${message}`);
  }
}
