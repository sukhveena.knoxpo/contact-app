export interface ILabel {
    id: number;
    name: string;
}

export interface IPhone {
    id: number;
    phonenumber?: string;
    code?: number;
    label?: number;
}

export interface IEmail {
    id: number;
    name?: string;
    label?: number;
}

export interface IContact {
    id: number;
    firstName: string;
    lastName?: string;
    middleName?: string;
    profilePhoto?: string;
    company?: string;
    job?: string;
    emailId?: IEmail[];
    phone?: IPhone[];
    notes?: string;
}
